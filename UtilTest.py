import TestUtil
import Util

value_list = [5,3,4,1,2]
TestUtil.assertEquals('Median calculation', 3, Util.calculateMedian(value_list))
value_list = [3,6,9,1,4]
TestUtil.assertEquals('Median calculation', 4, Util.calculateMedian(value_list))
value_list = [1,2,3,4,5]
TestUtil.assertEquals('Median calculation', 3, Util.calculateMedian(value_list))
value_list = [1,2,3,4,5,6]
TestUtil.assertEquals('Median calculation', 3.5, Util.calculateMedian(value_list))
value_list = [3,2,5,1,6,4]
TestUtil.assertEquals('Median calculation', 3.5, Util.calculateMedian(value_list))
value_list = [5,3,4,1,2,7,4,43,1,-3,-5]
TestUtil.assertEquals('Median calculation', 3, Util.calculateMedian(value_list))