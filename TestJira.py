import JiraHelpdeskTicketLinks
from JiraHelpdeskTicketLinks.jiraHelpdeskTicketLinks import calculateMedian

def assertEquals(strComment, expected_value, actual_value):
    if expected_value != actual_value:
        print strComment + ' Expected ' + str(expected_value) + ', but received ' + str(actual_value)

value_list = [5,3,4,1,2]
assertEquals('Median calculation', 3, calculateMedian(value_list))