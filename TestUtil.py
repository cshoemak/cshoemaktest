def assertEquals(strComment, expected_value, actual_value):
	if expected_value != actual_value:
		print '------------TEST FAILURE------------\n' + strComment + ' Expected <' + str(expected_value) + '>, but received <' + str(actual_value) + '>.'