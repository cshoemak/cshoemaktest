from jira.client import JIRA
from Auth import Cred
from datetime import datetime
import Util
# Adding a comment earlier in the file.

# A few more lines.
# And another

# Edit this line to represent JQL that will get your component tickets-----------
componentJQL = 'Filter = 12385 and Project in (PHC, IWEB) and issueType = "Bug"'
#--------------------------

helpdeskJQL = 'Filter = 13381' # This should probably not be changed

auth = Cred()
jira = JIRA(options = {'server': 'https://stchome.atlassian.net'}, basic_auth = (auth.username, auth.password))

#issue = jira.issue('PHC-544')
#print issue.fields.project.key
#print issue.fields.issuetype.name
#print issue.fields.reporter.displayName

def getLinkedIssues(issueThatMightHaveLinks):
	linkedIssues = []
	for issueLink in issueThatMightHaveLinks.fields.issuelinks:
		linkedIssue = None
		try:
			linkedIssue = issueLink.inwardIssue
		except AttributeError:
			try:
				linkedIssue = issueLink.outwardIssue
			except AttributeError:
				print 'I\'m completely lost and can\'t find the linked issue'
		if linkedIssue is not None:
			linkedIssues.append(linkedIssue)
	return linkedIssues

def calculateAgeInDays(issue):
#	print issue.key + ': ' + str(issue.fields.created)
	dt = datetime.strptime(issue.fields.created.split("T")[0], "%Y-%m-%d")
	today = datetime.today()
	age = today - dt
#	print 'Age: ' + str(age.days) + ' days'
	return age.days

def calculateMedianFromIssueList(issue_list):
	ages = []
	for issue in issue_list:
		ages.append(calculateAgeInDays(issue))
	return Util.calculateMedian(ages)

comp_filter_results = jira.search_issues(jql_str=componentJQL, maxResults=500, fields='issuelinks,summary,priority,created')

hd_linked_high = []
hd_linked_med = []
hd_linked_low = []
non_hd_linked_high = []
non_hd_linked_med = []
non_hd_linked_low = []

hd_filter_results = jira.search_issues(jql_str=helpdeskJQL, maxResults=500, fields='') #filter('17182')
for i,comp_ticket in enumerate(comp_filter_results):
	print comp_ticket.key + ': ' + comp_ticket.fields.summary
	priority = comp_ticket.fields.priority.name
	print 'Priority: ' + priority

#	print comp_ticket.fields.priority.raw
#	print dir(comp_ticket.fields.priority)
	hd_linked = False
	for j,linkedIssue in enumerate(getLinkedIssues(comp_ticket)):
		if j==0:
			print 'Linked Issues:'
		print linkedIssue.key
		for hd_ticket in hd_filter_results:
			if hd_ticket.key == linkedIssue.key:
				hd_linked = True
				break

	if priority == "Showstopper" or priority == 'High':
		if hd_linked:
			hd_linked_high.append(comp_ticket)
		else:
			non_hd_linked_high.append(comp_ticket)
	elif priority == 'Medium':
		if hd_linked:
			hd_linked_med.append(comp_ticket)
		else:
			non_hd_linked_med.append(comp_ticket)
	else:
		if hd_linked:
			hd_linked_low.append(comp_ticket)
		else:
			non_hd_linked_low.append(comp_ticket)


print '----------------All Tickets Linked to Helpdesk Bugs------------------------'
for issue in hd_linked_high:
	print issue.key + ": " + issue.fields.summary
	
for issue in hd_linked_med:
	print issue.key + ": " + issue.fields.summary
	
for issue in hd_linked_low:
	print issue.key + ": " + issue.fields.summary

print '----------------Totals and Median Ages------------------------'

print 'Showstopper/High HD tickets found: ' + str(len(hd_linked_high))
print 'Medium HD tickets found: ' + str(len(hd_linked_med))
print 'Low/Cosmetic HD tickets found: ' + str(len(hd_linked_low))

print 'Median age for HD linked high priority tickets: ' + str(calculateMedianFromIssueList(hd_linked_high))
print 'Median age for HD linked medium priority tickets: ' + str(calculateMedianFromIssueList(hd_linked_med))
print 'Median age for HD linked low priority tickets: ' + str(calculateMedianFromIssueList(hd_linked_low))

print 'Showstopper/High non-HD tickets found: ' + str(len(non_hd_linked_high))
print 'Medium non-HD tickets found: ' + str(len(non_hd_linked_med))
print 'Low/Cosmetic non-HD tickets found: ' + str(len(non_hd_linked_low))

print 'Median age for non-HD high priority tickets: ' + str(calculateMedianFromIssueList(non_hd_linked_high))
print 'Median age for non-HD medium priority tickets: ' + str(calculateMedianFromIssueList(non_hd_linked_med))
print 'Median age for non-HD low priority tickets: ' + str(calculateMedianFromIssueList(non_hd_linked_low))

# New line where you didn't expect it.

print 'I also want this code reviewed.' # Remove this line, will you?
# Conflicting signals.

# Conflicting signals.
