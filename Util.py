import math

def calculateMedian(value_list):
	list_len = len(value_list)
	if list_len < 1:
		return 0
	changed = True
	while changed:
		changed = False
		for i in range(list_len-1):
			if value_list[i] > value_list[i+1]:
				changed = True
				value1 = value_list[i]
				value_list[i] = value_list[i+1]
				value_list[i+1] = value1
	if list_len % 2 == 0:
		median_lower_index = list_len / 2 - 1
		median_upper_index = list_len / 2
		return float(value_list[median_upper_index] + value_list[median_lower_index]) / 2
	else:
		median_index = int(math.floor(list_len / 2))
#		print 'Median index: ' + str(median_index)
		return value_list[median_index]